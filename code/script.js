/*
У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: ""
}

let flag = false;
const display = document.querySelector(".display input");
const div = document.createElement('div');
div.classList.add('memor');
document.querySelector(".display").appendChild(div);



// https://regexr.com/

document.querySelector(".keys").addEventListener("click", (e) => {
    if(validate(/\d/, e.target.value) && calculate.sign == ""){
        calculate.operand1 += e.target.value;
        show(calculate.operand1);

    }else if(validate(/^[+-/*]$/, e.target.value) && calculate.operand2 == ''){
        calculate.sign = e.target.value;

    }else if(validate(/\d/, e.target.value)) { 
        calculate.operand2 += e.target.value; 
        show(calculate.operand2);
        document.querySelector(".orange").disabled = false;

    }else if (validate(/^=$/, e.target.value)){
        calculate.rez = calc(calculate.operand1, calculate.operand2, calculate.sign);
        show(calculate.rez);
        calculate.operand1 = calculate.rez;
        calculate.sign = '';
        calculate.operand2 = '';
        calculate.rez = '';
       
    }else if (validate(/^[+-/*]$/, e.target.value)){
        calculate.rez = calc(calculate.operand1, calculate.operand2, calculate.sign);
        show(calculate.rez);
        calculate.operand1 = calculate.rez;
        calculate.sign = '';
        calculate.operand2 = '';
        calculate.rez = '';

    }else if (e.target.value === 'C') {
        display.value = '';
        calculate.operand1 = '';
        calculate.operand2 = '';
        calculate.sign = '';
        document.querySelector('.memor').classList.add('memorNot');
        
       
    }else if (e.target.value === 'm+') {
            calculate.mem = Number(display.value) + Number(calculate.mem);
            div.innerHTML = 'm';
       
    }else if (e.target.value === 'm-') {
            calculate.mem -= display.value;
             div.innerHTML = 'm';
    
    }else if (e.target.value === 'mrc') {
        if (!flag) {
            show(calculate.mem);
            flag = true;
        } else {
            document.querySelector('.memor').classList.toggle('memorNot');
          calculate.mem = "";
        }}
       
    
    
console.log(calculate.operand1);
console.log(calculate.sign);
console.log(calculate.operand2);
console.log(calculate.rez);
console.log(calculate.mem);

    })


function show (v) {
    display.value = v;
}


const validate = (r, v) => r.test(v);

function calc (num1, num2, sign) {
   
    switch (sign) { 
        case '+' : return (+num1) + (+num2);
        case '-' : return num1 - num2;
        case '*' : return num1 * num2;
        case '/' : return num1 / num2;
    } 
}




